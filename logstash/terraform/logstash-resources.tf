resource "aws_security_group" "allow_http_api" {
  name        = "allow_http_api"
  description = "Allow traffic to HTTP API"
  vpc_id      = "${data.terraform_remote_state.networking.vpc_id}"

  ingress {
    from_port   = 9600
    to_port     = 9600
    protocol    = "tcp"
    self        = true
  }

  tags = "${merge(var.common_tags,
          map("globex:name", "logstash01-${terraform.workspace}"),
          map("globex:environment", "${terraform.workspace}"))}"
}

resource "aws_security_group" "beats" {
  name   = "logstash-beats-input"
  vpc_id = "${data.terraform_remote_state.networking.vpc_id}"

  ingress {
    from_port   = 5044
    to_port     = 5044
    protocol    = "tcp"
    self        = true
    security_groups = ["${data.aws_security_group.default.id}"]
    cidr_blocks = ["10.2.0.0/16"]
  }

  ingress {
    from_port   = 514
    to_port     = 514
    protocol    = "tcp"
    cidr_blocks = ["10.2.0.0/16"]
  }

  ingress {
    from_port   = 514
    to_port     = 514
    protocol    = "udp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = "${merge(var.common_tags,
          map("globex:name", "logstash01-${terraform.workspace}"),
          map("globex:environment", "${terraform.workspace}"))}"
}

resource "aws_instance" "logstash-host" {
  count         = 3
  instance_type = "t2.medium"
  ami           = "${data.aws_ami.centos.id}"
  key_name      = "${var.key_pair}"
  subnet_id     = "${element(data.aws_subnet_ids.cloud-logging.ids, count.index)}"
  vpc_security_group_ids = ["${aws_security_group.beats.id}", "${aws_security_group.allow_http_api.id}", "${data.aws_security_group.default.id}", "${data.terraform_remote_state.networking.ssh_sg_id}"]
  associate_public_ip_address = false
  lifecycle {
    ignore_changes = [
      "instance_type",
      "count"
    ]
  }

  tags = "${merge(var.common_tags,
          map("globex:name", "logstash01-${terraform.workspace}"),
          map("globex:environment", "${terraform.workspace}"),
          map("globex:application", "logstash"))}"
}

resource "aws_route53_record" "logstash-private" {
  count   = 3
  zone_id = "${data.aws_route53_zone.dtspur.zone_id}"
  name    = "internal-logstash-${count.index}.${data.aws_route53_zone.dtspur.name}"
  type    = "A"
  ttl     = "30"
  records = ["${aws_instance.logstash-host.*.private_ip[count.index]}"]
}


# resource "aws_launch_configuration" "logstash-launch-configuration" {
#   name_prefix          = "${terraform.workspace}-logstash-lc-"
#   image_id             = "${data.aws_ami.centos.id}"
#   instance_type        = "t2.medium"
#   security_groups      = ["${aws_security_group.beats.id}", "${aws_security_group.allow_http_api.id}", "${data.aws_security_group.default.id}", "${data.terraform_remote_state.networking.ssh_sg_id}"]

#   lifecycle {
#     create_before_destroy = true
#   }

#   associate_public_ip_address = "false"
#   key_name                    = "${var.key_pair}"
# }

# resource "aws_autoscaling_group" "logstash-autoscaling-group" {
#   name             = "logtash-autoscaling-group"
#   max_size         = "5"
#   min_size         = "2"
#   desired_capacity = "3"

#   vpc_zone_identifier  = ["${data.aws_subnet_ids.cloud-logging.ids}"]
#   launch_configuration = "${aws_launch_configuration.logstash-launch-configuration.name}"
#   health_check_type    = "ELB"

#   tags = [
#     {
#       key                 = "globex:name"
#       value               = "logstash01-${terraform.workspace}"
#       propagate_at_launch = true
#     },
#     {
#       key                 = "globex:project"
#       value               = "cloud-logging"
#       propagate_at_launch = true
#     },
#     {
#       key                 = "globex:contact"
#       value               = "chris.lord@dtspur.com"
#       propagate_at_launch = true
#     },
#     {
#       key                 = "globex:provisioner"
#       value               = "terraform"
#       propagate_at_launch = true
#     },
#     {
#       key                 = "globex:environment"
#       value               = "${terraform.workspace}"
#       propagate_at_launch = true
#     },
#     {
#       key                 = "globex:application"
#       value               = "logstash"
#       propagate_at_launch = true
#     }
#   ]
# }