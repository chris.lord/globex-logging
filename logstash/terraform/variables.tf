variable "common_tags" {
  type        = "map"
  description = "These tags will be appended to all resources created by terraform"

  default = {
    "globex:project"         = "cloud-logging"
    "globex:contact"         = "chris.lord@dtspur.com"
    "globex:team"            = "virtucon"
    "globex:provisioner"     = "terraform"
  }
}

variable "image_tag" {
  description = "Tag of the docker image to use in the ECS Task Definition. Defaults to latest."
  type        = "string"
  default     = "latest"
}

variable "key_pair" {
  type = "string"
  default = "cardno:000609544376"
}