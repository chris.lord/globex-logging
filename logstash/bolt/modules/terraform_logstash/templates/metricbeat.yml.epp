---
logging:
  level: info
  metrics:
    enabled: false
    period: 30s
  selectors: 
  to_files: false
  to_syslog: true

metricbeat.config:
  modules:
    path: ${path.config}/modules.d/*.yml
    # Reload module configs as they change:
    reload.enabled: false

metricbeat.autodiscover:
  providers:
    - type: docker
      hints.enabled: true
metricbeat.modules:
- module: docker
  metricsets:
    - "container"
    - "cpu"
    - "diskio"
    - "healthcheck"
    - "info"
    - "image"
    - "memory"
    - "network"
  hosts: ["unix:///var/run/docker.sock"]
  period: 10s
  enabled: true
- module: system
  metricsets:
    - cpu
    - load
    - memory
    - process
  processes:
    - ".*"
metricbeat.modules:
- module: logstash
  metricsets: ["node", "node_stats"]
  enabled: true
  period: 10s
  hosts: ["<%= $facts['ipaddress'] -%>:9600"]
processors:
  - add_cloud_metadata: ~

output.logstash.hosts:
<% $logstash_list.each | $instance | {-%>
  - <%= $instance %>
<% } -%>
xpack.monitoring:
  enabled: true
  elasticsearch:
    hosts:
  <% $elastic_list.each | $elastic | { -%>
      - <%= $elastic %>
  <% } -%>
