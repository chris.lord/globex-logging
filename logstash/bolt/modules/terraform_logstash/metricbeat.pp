plan terraform_logstash::metricbeat (
  TargetSpec $nodes,
  Array $logstash_list
) {
  apply($nodes, _run_as => 'root') {
    docker::image { 'docker.elastic.co/beats/metricbeat':
      image_tag => '7.3.0'
    }
  }

  apply($nodes, _run_as => 'root') {

    file { '/etc/metricbeat':
      ensure => directory
    }

    -> file { '/etc/metricbeat/metricbeat.yml':
      ensure  => file,
      content => epp('terraform_metricbeat/metricbeat.yml.epp'),
      mode    => '0777'
    }

    ~> docker::run { 'metricbeat':
      image            => 'docker.elastic.co/metricbeat/metricbeat:7.3.0',
      service_prefix   => '',
      net              => 'host',
      volumes          => ['/etc/metricbeat:/usr/share/metricbeat'],
      restart_service  => true,
      privileged       => false,
      pull_on_start    => false,
      stop_wait_time   => 0,
      read_only        => false,
      extra_parameters => [ '--restart=always',
        '--mount type=bind,source=/proc,target=/hostfs/proc,readonly', 
        '--mount type=bind,source=/sys/fs/cgroup,target=/hostfs/sys/fs/cgroup,readonly',
        '--mount type=bind,source=/,target=/hostfs,readonly' ],
    }
}
