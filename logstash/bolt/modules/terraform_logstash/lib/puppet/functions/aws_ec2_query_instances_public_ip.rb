require 'rubygems'

require 'aws-sdk'

Puppet::Functions.create_function(:aws_ec2_query_instances_public_ip) do
    dispatch :query do
        param 'Array[Data]', :tags
        param 'String', :region
    end

    def query(tags, region)

        # Define two arrays needed for munging data
        ip_list = Array.new
        filter = Array.new

        # For each tag provided to the function make them suitable to pass to
        # the describe_instance call and push them to a filter list
        tags.each { | element | 
            element.each { | key, value |
                filter.push({name: key, values: [value]})
            }
        }


        ec2 = Aws::EC2::Client.new(
            region: region
        )

        response = ec2.describe_instances({
            filters: filter
        })

        # Pull the list of instances out of the response.
        instances = response.reservations.map(&:instances)

        # This is full of symbols as hash keys which makes it impossible to use
        # in puppet. We're forced to return ONLY the public IP address instead
        # of all of the data.
        instances.each { |value|
            value.each { |inner_value|
                ip = inner_value.dig(:public_ip_address)
                if ip
                    ip_list.push(ip)
                end
            }
        }
        ip_list        
        
    end
end
