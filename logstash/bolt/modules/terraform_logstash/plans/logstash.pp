plan terraform_logstash::logstash (
  TargetSpec $nodes,
  Array $instance_list,
  String $log_level = 'info',
  Array $elastic_list,
  Array $logstash_list,
  Array $all_instance_list,
  String $community = 'globex-logging'
) {

  apply($nodes, _run_as => 'root') {
    docker::image { 'docker.elastic.co/logstash/logstash':
      image_tag => '7.4.0'
    }

    class { 'filebeat':
      major_version  => '7',
      package_ensure => 'latest',
      outputs        => {
        'logstash' => {
          'hosts'                       => $logstash_list,
          'loadbalance'                 => true,
        },
      },
      xpack          => {
        'monitoring' => {
          'enabled'       => true,
          'elasticsearch' => {
            'hosts' => $elastic_list
          }
        }
      },
      modules        => [
        {
          'module' => 'system',
          'syslog'  => {
            'enabled' => 'true',
            'var.paths' => ['/var/log/messages']
          },
          'auth'    => {
            'enabled' => 'true',
            'var.paths' => ['/var/log/secure']
          }
        }
      ]
    }

    class {'metricbeat':
      major_version => '7',
      disable_configtest => true,
      modules       => [
        {
          'module'     => 'system',
          'metricsets' => [
            'cpu',
            'load',
            'memory',
            'network',
            'process_summary',
            'process',
            'uptime',
            'socket_summary',
            'core',
            'filesystem',
            'diskio',
            'fsstat',
            'socket'
          ],
          'processes'  => ['.*'],
        },
        {
          'module'     => 'docker',
          'metricsets' => [
            'container',
            'cpu',
            'diskio',
            'event',
            'healthcheck',
            'info',
            'image',
            'memory',
            'network'
          ],
          'hosts'      => [
            'unix:///var/run/docker.sock'
          ]
        }
      ],
      outputs       => {
        'logstash' => {
          'hosts'                       => $logstash_list,
          'loadbalance'                 => true,
        },
      },
      xpack         => {
        'monitoring' => {
          'enabled'       => true,
          'elasticsearch' => {
            'hosts' => $elastic_list
          }
        }
      }
    }

    file { '/etc/journalbeat':
      ensure => directory
    }

    -> file { '/etc/journalbeat/journalbeat.yml':
      ensure  => file,
      content => epp('terraform_logstash/journalbeat.yml.epp'),
      mode    => '0644'
    }

    -> package { 'journalbeat':
      ensure => present,
    }

    ~> service { 'journalbeat':
      ensure => running
    }

    file { '/etc/logstash':
      ensure => directory
    }

    -> file { '/etc/logstash/config':
      ensure => directory,
    }

    -> file { '/etc/logstash/config/logstash.yml':
      ensure  => file,
      content => epp('terraform_logstash/logstash.yml.epp'),
      mode    => '0644'
    }

    -> file { '/etc/logstash/config/pipelines.yml':
      ensure  => file,
      content => epp('terraform_logstash/pipelines.yml.epp'),
      mode    => '0644'
    }

    -> file { '/etc/logstash/pipeline':
      ensure => directory,
      mode   => '0755'
    }

    -> file { '/etc/logstash/pipeline/00-input.conf':
      ensure  => file,
      content => epp('terraform_logstash/00-input.conf.epp'),
      mode    => '0644'
    }

    -> file { '/etc/logstash/pipeline/96-snmp-output.conf':
      ensure  => file,
      content => epp('terraform_logstash/96-snmp-output.conf.epp'),
      mode    => '0644'
    }

    -> file { '/etc/logstash/pipeline/97-pfsense-output.conf':
      ensure  => file,
      content => epp('terraform_logstash/97-pfsense-output.conf.epp'),
      mode    => '0644'
    }

    -> file { '/etc/logstash/pipeline/98-syslog-output.conf':
      ensure  => file,
      content => epp('terraform_logstash/98-syslog-output.conf.epp'),
      mode    => '0644'
    }

    -> file { '/etc/logstash/pipeline/99-output.conf':
      ensure  => file,
      content => epp('terraform_logstash/99-output.conf.epp'),
      mode    => '0644'
    }

    -> file { '/etc/logstash/pipeline/50-pfsense-filter.conf':
      ensure  => file,
      content => epp('terraform_logstash/50-pfsense-filter.conf.epp'),
      mode    => '0644'
    }

    ~> docker::run { 'logstash':
      image            => 'docker.elastic.co/logstash/logstash:7.4.0',
      service_prefix   => '',
      net              => 'host',
      volumes          => ['/etc/logstash/config:/usr/share/logstash/config/',
      '/etc/logstash/pipeline:/usr/share/logstash/pipeline',
      '/etc/logstash/ca.crt:/usr/share/logstash/ca.crt',
      '/etc/logstash/server.crt:/usr/share/logstash/server.crt',
      '/etc/logstash/server.key:/usr/share/logstash/server.key'],
      restart_service  => true,
      privileged       => true,
      pull_on_start    => false,
      stop_wait_time   => 0,
      read_only        => false,
      extra_parameters => [ '--restart=always',
        '--label co.elastic.metrics/module=logstash',
  '--label co.elastic.metrics/metricsets=node,node_stats',
  "--label co.elastic.metrics/hosts='${facts['ipaddress']}:9600'",
  '--user=root'
  ],
      env              => ["LS_JAVA_OPTS=-Xmx2G -Xms2G",],
      command          => "/usr/share/logstash/bin/logstash"
    }
  }

upload_file('/home/chrisl/projects/globex-logging/logstash/bolt/modules/terraform_logstash/files/patterns', '/etc/logstash/config/patterns', $nodes, '_run_as' => 'root')

}
