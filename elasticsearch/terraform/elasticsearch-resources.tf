resource "aws_security_group" "allow_elastic_http_api" {
  name        = "allow_elastic_http_api"
  description = "Allow traffic to HTTP API"
  vpc_id      = "${data.terraform_remote_state.networking.vpc_id}"

  ingress {
    from_port   = 9200
    to_port     = 9200
    protocol    = "tcp"
    self        = true
    security_groups = ["${data.aws_security_group.default.id}"]
  }

  tags = "${merge(var.common_tags,
          map("globex:name", "elasticsearch01-${terraform.workspace}"),
          map("globex:environment", "${terraform.workspace}"))}"
}

resource "aws_security_group" "allow_elastic_transport_api" {
  name        = "allow_elastic_transport_api"
  description = "Allow traffic to transport API"
  vpc_id      = "${data.terraform_remote_state.networking.vpc_id}"

  ingress {
    from_port   = 9300
    to_port     = 9300
    protocol    = "tcp"
    self        = true
  }

  tags = "${merge(var.common_tags,
          map("globex:name", "elasticsearch01-${terraform.workspace}"),
          map("globex:environment", "${terraform.workspace}"))}"
}

resource "aws_iam_role" "s3_access" {
  name = "s3_access"
  path = "/"

  assume_role_policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Action": "sts:AssumeRole",
            "Principal": {
               "Service": "ec2.amazonaws.com"
            },
            "Effect": "Allow"
        }
    ]
}
EOF
}

resource "aws_iam_role_policy" "s3_access" {
  name = "s3_access"
  role = "${aws_iam_role.s3_access.id}"

  policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "AllowListAndReadS3ActionOnMyBucket",
            "Effect": "Allow",
            "Action": [
                "s3:Get*",
                "s3:List*",
                "s3:PutObject",
                "s3:DeleteObject",
                "s3:AbortMultipartUpload",
                "s3:ListMultipartUploadParts"
            ],
            "Resource": [
                "arn:aws:s3:::globex-cloud-logging-backups/*",
                "arn:aws:s3:::globex-cloud-logging-backups"
            ]
        }
    ]
}
EOF
}

resource "aws_iam_instance_profile" "s3_access" {
  name = "s3_access"
  role = "${aws_iam_role.s3_access.name}"
}

# resource "aws_ebs_volume" "elastic-volume" {
#   count = 3
#   availability_zone = "${element(var.azs, count.index)}"
#   size              = 500
# }

resource "aws_instance" "elastic-host" {
  count         = 3
  instance_type = "m5.large"
  ami           = "${data.aws_ami.centos.id}"
  key_name      = "${var.key_pair}"
  subnet_id     = "${element(data.aws_subnet_ids.cloud-logging.ids, count.index)}"
  vpc_security_group_ids = ["${data.aws_security_group.default.id}","${aws_security_group.allow_elastic_transport_api.id}", "${aws_security_group.allow_elastic_http_api.id}", "${data.terraform_remote_state.networking.ssh_sg_id}"]
  associate_public_ip_address = false
  iam_instance_profile = "${aws_iam_instance_profile.s3_access.id}"

  lifecycle {
    ignore_changes = [
      "instance_type",
      "count"
    ]
  }

  tags = "${merge(var.common_tags,
          map("globex:name", "elasticsearch01-${terraform.workspace}"),
          map("globex:environment", "${terraform.workspace}"),
          map("globex:application", "elasticsearch"))}"
}

# resource "aws_volume_attachment" "elastic-volume-attach" {
#   count = 3
#   device_name = "/dev/sdh"
#   volume_id   = "${aws_ebs_volume.elastic-volume.*.id[count.index]}"
#   instance_id = "${aws_instance.elastic-host.*.id[count.index]}"
# }

resource "aws_route53_record" "elasticsearch" {
  count   = 3
  zone_id = "${data.aws_route53_zone.dtspur.zone_id}"
  name    = "elasticsearch-${count.index}.${data.aws_route53_zone.dtspur.name}"
  type    = "A"
  ttl     = "30"
  records = ["${aws_instance.elastic-host.*.private_ip[count.index]}"]
}

resource "aws_s3_bucket" "elastic_backups" {
  bucket = "globex-cloud-logging-backups"
  acl    = "private"
  policy = <<POLICY
{
  "Statement": [
    {
      "Action": [
        "s3:ListBucket",
        "s3:GetBucketLocation",
        "s3:ListBucketMultipartUploads",
        "s3:ListBucketVersions"
      ],
      "Effect": "Allow",
      "Principal": "*",
      "Condition": {
         "IpAddress": {"aws:SourceIp": "10.3.0.0/16"}
      },
      "Resource": [
        "arn:aws:s3:::globex-cloud-logging-backups"
      ]
    },
    {
      "Action": [
        "s3:GetObject",
        "s3:PutObject",
        "s3:DeleteObject",
        "s3:AbortMultipartUpload",
        "s3:ListMultipartUploadParts"
      ],
      "Effect": "Allow",
      "Principal": "*",
      "Condition": {
         "IpAddress": {"aws:SourceIp": "10.3.0.0/16"}
      },
      "Resource": [
        "arn:aws:s3:::globex-cloud-logging-backups/*"
      ]
    }
  ],
  "Version": "2012-10-17"
}
POLICY

  tags = "${merge(var.common_tags,
          map("globex:name", "elasticsearch01-${terraform.workspace}"),
          map("globex:environment", "${terraform.workspace}"),
          map("globex:application", "elasticsearch"))}"
}