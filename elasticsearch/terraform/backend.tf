terraform {
  backend "s3" {
    bucket = "globex-terraform-remote-state"
    key    = "cloud-logging/elasticsearch/tfstate"
    region = "ap-southeast-2"
  }
}