plan terraform_elastic::elasticsearch (
  TargetSpec $nodes,
  Array $instance_list,
  String $log_level = 'info',
  Array $logstash_list,
) {

  apply($nodes, _run_as => 'root') {

    file { '/tmp/dockerfile':
      ensure => present,
      source => 'puppet:///modules/terraform_elastic/dockerfile'
    }

    docker::image { 'elasticsearch':
      docker_file => '/tmp/dockerfile'
    }

    class { 'filebeat':
      major_version  => '7',
      package_ensure => 'latest',
      outputs        => {
        'logstash' => {
          'hosts'       => $logstash_list,
          'loadbalance' => true,
        },
      },
      xpack          => {
        'monitoring' => {
          'enabled'       => true,
          'elasticsearch' => {
            'hosts' => suffix($instance_list, ':9200')
          }
        }
      },
      modules        => [
        {
          'module' => 'system',
          'syslog' => {
            'enabled'   => true,
            'var.paths' => ['/var/log/messages']
          },
          'auth'   => {
            'enabled'   => true,
            'var.paths' => ['/var/log/secure']
          }
        },
        {
          'module' => 'elasticsearch',
          'server'  => {
            'enabled'   => true,
            'var.paths' => [
              '/var/log/elasticsearch/*_server.json'
            ]
          },
          'gc' => {
            'var.paths' => [
              '/var/log/elasticsearch/gc.log.[0-9]*',
              '/var/log/elasticsearch/gc.log'
            ]
          },
          'audit' => {
            'var.paths' => [
              '/var/log/elasticsearch/*_audit.json'
            ]
          },
          'slowlog' => {
            'var.paths' => [
              '/var/log/elasticsearch/*_index_search_slowlog.json',
              '/var/log/elasticsearch/*_index_indexing_slowlog.json'
            ]
          }
        }
      ]
    }

    exec { 'enable elasticsearch module':
      path    => ['/usr/bin'],
      command => 'filebeat modules enable elasticsearch'
    }

    class {'metricbeat':
      major_version => '7',
      disable_configtest => true,
      package_ensure => 'latest',
      modules       => [
        {
          'module'     => 'system',
          'metricsets' => [
            'cpu',
            'load',
            'memory',
            'network',
            'process_summary',
            'process',
            'uptime',
            'socket_summary',
            'core',
            'filesystem',
            'diskio',
            'fsstat',
            'socket'
          ],
          'processes'  => ['.*'],
        },
        {
          'module'     => 'docker',
          'metricsets' => [
            'container',
            'cpu',
            'diskio',
            'event',
            'healthcheck',
            'info',
            'image',
            'memory',
            'network'
          ],
          'hosts' => ['unix:///var/run/docker.sock']
        }
      ],
      outputs       => {
        'logstash' => {
          'hosts'       => $logstash_list,
          'loadbalance' => true,
        },
      },
      xpack         => {
        'monitoring' => {
          'enabled'       => true,
          'elasticsearch' => {
            'hosts' => suffix($instance_list, ':9200')
          }
        }
      }
    }

    file { '/etc/journalbeat':
      ensure => directory
    }

    -> file { '/etc/journalbeat/journalbeat.yml':
      ensure  => file,
      content => epp('terraform_elastic/journalbeat.yml.epp'),
      mode    => '0644'
    }

    -> package { 'journalbeat':
      ensure => present,
    }

    ~> service { 'journalbeat':
      ensure => running
    }

    file { '/etc/elasticsearch':
      ensure => directory
    }

    -> file { '/etc/elasticsearch/elasticsearch.yml':
      ensure  => file,
      content => epp('terraform_elastic/elasticsearch.yml.epp'),
      mode    => '0777'
    }

    ~> docker::run { 'elasticsearch':
      image            => 'elasticsearch',
      service_prefix   => '',
      net              => 'host',
      volumes          => ['/etc/elasticsearch/elasticsearch.yml:/usr/share/elasticsearch/config/elasticsearch.yml'],
      restart_service  => true,
      privileged       => false,
      pull_on_start    => false,
      stop_wait_time   => 0,
      read_only        => false,
      extra_parameters => [ '--restart=always' ],
      env              => ['ES_JAVA_OPTS=-Xmx4G -Xms4G',],
      command          => '/usr/share/elasticsearch/bin/elasticsearch'
    }
  }

}
