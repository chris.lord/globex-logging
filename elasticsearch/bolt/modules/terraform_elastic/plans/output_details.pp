plan terraform_elastic::output_details (
  String $application
) {
  $_instance_list = aws_ec2_query_instances_public_ip([{"tag:Project" => "elastic-test"},{"tag:Application" => $application}], 'ap-southeast-2')
  out::message($_instance_list.join(','))
  $_private_instance_list = aws_ec2_query_instances_private_ip([{"tag:Project" => "elastic-test"},{"tag:Application" => $application}], 'ap-southeast-2')
  out::message($_private_instance_list.join(','))
}
