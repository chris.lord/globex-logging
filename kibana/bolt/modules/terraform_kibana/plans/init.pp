plan terraform_kibana (
  String $tf_path = 'terraform',
  Boolean $plan_tf = false,
  Boolean $apply_tf = false
) {
  $localhost = get_targets('localhost')


  if $plan_tf or $apply_tf {
    run_command("cd ${tf_path} && /usr/local/bin/terraform plan -out=planfile",
            $localhost, '_catch_errors' => false)
  }

  if $apply_tf {
    run_command("cd ${tf_path} && /usr/local/bin/terraform apply planfile",
                $localhost, '_catch_errors' => false)
  }

  # Fetch kibana instances and create targets
  $kibana_instance_list = aws_ec2_query_instances_private_ip([
    {'tag:globex:project' => 'cloud-logging'},
    {'tag:globex:application' => 'kibana'}
    ], 'ap-southeast-2')
  $kibana_instance_list.map | $ip | {
    if $ip {
      Target.new($ip, 'transport'=>'ssh', 'user'=>'centos', 'run_as'=>'root').add_to_group('kibana')
    }
  }

  $elastic_instance_list = aws_ec2_query_instances_private_ip([
    {'tag:globex:project' => 'cloud-logging'},
    {'tag:globex:application' => 'elasticsearch'}
    ], 'ap-southeast-2')

  $elastic_host_suffix = suffix($elastic_instance_list, ':9200')
  $elastic_host_array = prefix($elastic_host_suffix, 'http://')

  $logstash_instance_list = aws_ec2_query_instances_private_ip([
  {'tag:globex:project' => 'cloud-logging'},
  {'tag:globex:application' => 'logstash'}
  ], 'ap-southeast-2')

  $logstash_host_suffix = suffix($logstash_instance_list, ':5044')
  $logstash_host_array = prefix($logstash_host_suffix, '')

  # Install puppet on each node
  run_task('puppet_agent::install', 'kibana', _run_as => 'root')
  apply_prep('kibana')

  # Install some useful packages and docker
  apply('kibana', _run_as => 'root') {
    package { ['vim-enhanced', 'nmap-ncat', 'epel-release', 'htop']:
      ensure => latest
    }

    class { 'docker':
      manage_package => true,
      version        => latest,
      docker_users   => ['centos']
    }

    class { 'snmp':
      agentaddress => [ 'udp:161', ],
      ro_community => 'globex-logging',
      ro_network   => '10.3.0.0/16',
    }


    Package['epel-release'] -> Package <| title != 'epel-release' |>
  }

  run_plan('terraform_kibana::kibana', 'nodes' => 'kibana', 'instance_list' => $kibana_instance_list, 'elastic_list' => $elastic_host_array, 'logstash_list' => $logstash_host_array)
}
