plan terraform_kibana::kibana (
  TargetSpec $nodes,
  Array $instance_list,
  String $log_level = 'info',
  Array $elastic_list,
  Array $logstash_list
) {

  apply($nodes, _run_as => 'root') {
    docker::image { 'docker.elastic.co/kibana/kibana':
      image_tag => '7.4.0'
    }

    class { 'filebeat':
      major_version  => '7',
      package_ensure => 'latest',
      outputs        => {
        'logstash' => {
          'hosts'       => $logstash_list,
          'loadbalance' => true,
        },
      },
      xpack          => {
        'monitoring' => {
          'enabled'       => true,
          'elasticsearch' => {
            'hosts' => $elastic_list
          }
        }
      },
      modules        => [
        {
          'module' => 'system',
          'syslog'  => {
            'enabled' => true,
            'var.paths' => ['/var/log/messages']
          },
          'auth'    => {
            'enabled' => true,
            'var.paths' => ['/var/log/secure']
          }
        }
      ]
    }

    class {'metricbeat':
      major_version => '7',
      disable_configtest => true,
      package_ensure => 'latest',
      modules       => [
        {
          'module'     => 'system',
          'metricsets' => [
            'cpu',
            'load',
            'memory',
            'network',
            'process_summary',
            'process',
            'uptime',
            'socket_summary',
            'core',
            'filesystem',
            'diskio',
            'fsstat',
            'socket'
          ],
          'processes'  => ['.*'],
        },
        {
          'module'     => 'docker',
          'metricsets' => [
            'container',
            'cpu',
            'diskio',
            'event',
            'healthcheck',
            'info',
            'image',
            'memory',
            'network'
          ],
          'hosts' => ['unix:///var/run/docker.sock']
        }
      ],
      outputs       => {
        'logstash' => {
          'hosts'       => $logstash_list,
          'loadbalance' => true,
        },
      },
      xpack         => {
        'monitoring' => {
          'enabled'       => true,
          'elasticsearch' => {
            'hosts' => $elastic_list
          }
        }
      }
    }

    file { '/etc/journalbeat':
      ensure => directory
    }

    -> file { '/etc/journalbeat/journalbeat.yml':
      ensure  => file,
      content => epp('terraform_kibana/journalbeat.yml.epp'),
      mode    => '0644'
    }

    -> package { 'journalbeat':
      ensure => present,
    }

    ~> service { 'journalbeat':
      ensure => running
    }

    file { '/etc/kibana':
      ensure => directory
    }

    -> file { '/etc/kibana/config':
      ensure => directory,
    }

    -> file { '/etc/kibana/config/kibana.yml':
      ensure  => file,
      content => epp('terraform_kibana/kibana.yml.epp'),
      mode    => '0777'
    }

    ~> docker::run { 'kibana':
      image            => 'docker.elastic.co/kibana/kibana:7.4.0',
      service_prefix   => '',
      net              => 'host',
      volumes          => ['/etc/kibana/config:/usr/share/kibana/config/'],
      restart_service  => true,
      privileged       => false,
      pull_on_start    => false,
      stop_wait_time   => 0,
      read_only        => false,
      extra_parameters => [ '--restart=always' ],
      env              => ["ES_JAVA_OPTS=-Xmx1G -Xms1G",]
    }
  }

}
