resource "aws_security_group" "allow_http_api" {
  name        = "allow_kibana_http_api"
  description = "Allow traffic to HTTP API"
  vpc_id      = "${data.terraform_remote_state.networking.vpc_id}"

  ingress {
    from_port   = 5601
    to_port     = 5601
    protocol    = "tcp"
    self        = true
    cidr_blocks = ["0.0.0.0/0"]
    security_groups = ["${data.aws_security_group.default.id}"]
  }

  tags = "${merge(var.common_tags,
          map("globex:name", "logstash01-${terraform.workspace}"),
          map("globex:environment", "${terraform.workspace}"))}"
}

resource "aws_instance" "kibana-host" {
  instance_type = "t2.medium"
  ami           = "${data.aws_ami.centos.id}"
  key_name      = "${var.key_pair}"
  subnet_id     = "${element(data.aws_subnet_ids.cloud-logging.ids, 0)}"
  vpc_security_group_ids = ["${data.terraform_remote_state.networking.ssh_sg_id}", "${data.aws_security_group.default.id}", "${aws_security_group.allow_http_api.id}"]
  associate_public_ip_address = false

  

  tags = "${merge(var.common_tags,
          map("globex:name", "kibana01-${terraform.workspace}"),
          map("globex:environment", "${terraform.workspace}"))}"
}

resource "aws_route53_record" "kibana" {
  zone_id = "${data.aws_route53_zone.dtspur.zone_id}"
  name    = "kibana.${data.aws_route53_zone.dtspur.name}"
  type    = "A"
  ttl     = "30"
  records = ["${aws_instance.kibana-host.private_ip}"]
}