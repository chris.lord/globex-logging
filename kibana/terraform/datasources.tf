data "terraform_remote_state" "networking" {
  backend   = "s3"

  config {
    bucket = "globex-terraform-remote-state"
    region = "ap-southeast-2"
    key    = "networking/tfstate"
  }
}

data "aws_ami" "centos" {
owners      = ["679593333241"]
most_recent = true

  filter {
      name   = "name"
      values = ["CentOS Linux 7 x86_64 HVM EBS *"]
  }

  filter {
      name   = "architecture"
      values = ["x86_64"]
  }

  filter {
      name   = "root-device-type"
      values = ["ebs"]
  }
}


data "aws_subnet_ids" "cloud-logging" {
  vpc_id = "${data.terraform_remote_state.networking.vpc_id}"

  tags = {
    "globex:project" = "cloud-logging"
    "globex:subnet-type" = "private"
  }
}

data "aws_subnet_ids" "cloud-logging-public" {
  vpc_id = "${data.terraform_remote_state.networking.vpc_id}"

  tags = {
    "globex:project" = "cloud-logging"
    "globex:subnet-type" = "public"
  } 
}

data "aws_security_group" "default" {
  name = "default"
}

data "aws_security_group" "allow_elastic_http_api" {
  name = "allow_elastic_http_api"
}

data "aws_route53_zone" "dtspur" {
  name = "dtspur.com."
}