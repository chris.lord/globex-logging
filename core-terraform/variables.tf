variable "common_tags" {
  type        = "map"
  description = "These tags will be appended to all resources created by terraform"

  default = {
    "globex:project"         = "cloud-logging"
    "globex:contact"         = "chris.lord@dtspur.com"
    "globex:team"            = "virtucon"
    "globex:provisioner"     = "terraform"
  }
}

variable "key_pair" {
  type = "string"
  default = "cardno:000609544376"
}