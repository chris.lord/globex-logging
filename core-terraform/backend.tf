terraform {
  backend "s3" {
    bucket = "globex-terraform-remote-state"
    key    = "networking/tfstate"
    region = "ap-southeast-2"
  }
}