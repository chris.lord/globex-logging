resource "aws_vpc" "cloud-logging" {
  cidr_block = "10.3.0.0/16"
}

output "vpc_id" {
  value = "${aws_vpc.cloud-logging.id}"
}

resource "aws_internet_gateway" "gw" {
  vpc_id = "${aws_vpc.cloud-logging.id}"

  tags = "${merge(var.common_tags,
          map("globex:name", "globex-igw"),
          map("globex:environment", "${terraform.workspace}"))}"
}

resource "aws_eip" "nat" {
  vpc = true
}

data "aws_security_group" "default" {
  name = "default"
}

resource "aws_security_group_rule" "allow_snmp" {
  type            = "ingress"
  from_port       = 161
  to_port         = 162
  protocol        = "udp"
  # Opening to 0.0.0.0/0 can lead to security vulnerabilities.
  cidr_blocks = ["10.3.0.0/16"]

  security_group_id = "${data.aws_security_group.default.id}"
}

resource "aws_security_group" "allow-ssh-public" {
  name        = "allow-ssh-public"
  description = "Allow SSH into the jump host"
  vpc_id      = "${aws_vpc.cloud-logging.id}"

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = "${merge(var.common_tags,
          map("globex:name", "allow-ssh-public"),
          map("globex:environment", "${terraform.workspace}"))}"
}

resource "aws_security_group" "allow-ssh" {
  name        = "allow-ssh"
  description = "Allow SSH from the jump host"
  vpc_id      = "${aws_vpc.cloud-logging.id}"

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    self        = true
  }

  egress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    self        = true
  }

  tags = "${merge(var.common_tags,
          map("globex:name", "allow-ssh"),
          map("globex:environment", "${terraform.workspace}"))}"
}

output "ssh_sg_id" {
  value = "${aws_security_group.allow-ssh.id}"
}

resource "aws_security_group" "allow-vpn-public" {
  name        = "allow-vpn-public"
  description = "Allow VPN into the VPN host"
  vpc_id      = "${aws_vpc.cloud-logging.id}"

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    security_groups = ["${aws_security_group.allow-ssh.id}"]
    self = true
  }

  ingress {
    from_port = 1194
    to_port = 1194
    protocol = "udp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port = 443
    to_port = 443
    protocol = "tcp"
    self = true
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port = 943
    to_port = 943
    protocol = "tcp"
    self = true
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port = 945
    to_port = 945
    protocol = "tcp"
    self = true
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = "${merge(var.common_tags,
          map("globex:name", "allow-vpn-public"),
          map("globex:environment", "${terraform.workspace}"))}"
}

resource "aws_subnet" "cloud-logging-public" {
  vpc_id     = "${aws_vpc.cloud-logging.id}"
  cidr_block = "10.3.0.0/28"
  availability_zone = "ap-southeast-2a"

  tags = "${merge(var.common_tags,
          map("globex:subnet-type", "public"))}"
}

resource "aws_nat_gateway" "gw" {
  allocation_id = "${aws_eip.nat.id}"
  subnet_id     = "${aws_subnet.cloud-logging-public.id}"
}


resource "aws_subnet" "cloud-logging-a" {
  vpc_id     = "${aws_vpc.cloud-logging.id}"
  cidr_block = "10.3.1.0/28"
  availability_zone = "ap-southeast-2a"

  tags = "${merge(var.common_tags,
          map("globex:subnet-type", "private"))}"
}

resource "aws_subnet" "cloud-logging-b" {
  vpc_id     = "${aws_vpc.cloud-logging.id}"
  cidr_block = "10.3.1.16/28"
  availability_zone = "ap-southeast-2b"

  tags = "${merge(var.common_tags,
          map("globex:subnet-type", "private"))}"
}

resource "aws_subnet" "cloud-logging-c" {
  vpc_id     = "${aws_vpc.cloud-logging.id}"
  cidr_block = "10.3.1.32/28"
  availability_zone = "ap-southeast-2c"

  tags = "${merge(var.common_tags,
          map("globex:subnet-type", "private"))}"
}

resource "aws_route_table" "public" {
  vpc_id = "${aws_vpc.cloud-logging.id}"

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = "${aws_internet_gateway.gw.id}"
  }

  tags = "${merge(var.common_tags,
          map("globex:gateway-type", "public"))}"
}

resource "aws_route_table" "private" {
  vpc_id = "${aws_vpc.cloud-logging.id}"

  route {
    cidr_block = "0.0.0.0/0"
    nat_gateway_id = "${aws_nat_gateway.gw.id}"
  }

  route {
    cidr_block = "10.2.0.0/16"
    nat_gateway_id = "${aws_nat_gateway.gw.id}"
  }

  tags = "${merge(var.common_tags,
          map("globex:gateway-type", "private"))}"
}

resource "aws_route_table_association" "public" {
  subnet_id      = "${aws_subnet.cloud-logging-public.id}"
  route_table_id = "${aws_route_table.public.id}"
}

resource "aws_route_table_association" "private_a" {
  subnet_id      = "${aws_subnet.cloud-logging-a.id}"
  route_table_id = "${aws_route_table.private.id}"
}

resource "aws_route_table_association" "private_b" {
  subnet_id      = "${aws_subnet.cloud-logging-b.id}"
  route_table_id = "${aws_route_table.private.id}"
}

resource "aws_route_table_association" "private_c" {
  subnet_id      = "${aws_subnet.cloud-logging-c.id}"
  route_table_id = "${aws_route_table.private.id}"
}

resource "aws_instance" "jump-host" {
  instance_type = "t2.medium"
  ami           = "${data.aws_ami.centos.id}"
  key_name      = "${var.key_pair}"
  subnet_id     = "${aws_subnet.cloud-logging-public.id}"
  vpc_security_group_ids = ["${aws_security_group.allow-ssh.id}", "${aws_security_group.allow-ssh-public.id}"]
  associate_public_ip_address = true

  tags = "${merge(var.common_tags,
          map("globex:name", "jump01-${terraform.workspace}"),
          map("globex:environment", "${terraform.workspace}"))}"
}

resource "aws_route53_record" "jump" {
  zone_id = "${data.aws_route53_zone.dtspur.zone_id}"
  name    = "jump.${data.aws_route53_zone.dtspur.name}"
  type    = "A"
  ttl     = "30"
  records = ["${aws_instance.jump-host.public_ip}"]
}

resource "aws_instance" "openvpn-host" {
  instance_type = "t2.medium"
  ami           = "ami-0867af34a3abcc452"
  key_name      = "cardno:000609544376"
  subnet_id     = "${aws_subnet.cloud-logging-public.id}"
  vpc_security_group_ids = ["${aws_security_group.allow-ssh.id}", "${aws_security_group.allow-vpn-public.id}"]
  associate_public_ip_address = true

  tags = "${merge(var.common_tags,
          map("globex:name", "vpn01-${terraform.workspace}"),
          map("globex:environment", "${terraform.workspace}"))}"
}

resource "aws_route53_record" "openvpn" {
  zone_id = "${data.aws_route53_zone.dtspur.zone_id}"
  name    = "openvpn.${data.aws_route53_zone.dtspur.name}"
  type    = "A"
  ttl     = "30"
  records = ["${aws_instance.openvpn-host.public_ip}"]
}